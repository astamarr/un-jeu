﻿using Assets.Characters;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Players
{
	public class PlayerController : MonoBehaviour
	{
		public InputBindings InputBindings;
		public SphereCharacter ActiveCharacter;
		public NetworkIdentity NetworkIdentity;

		public void Update()
		{
			if ((NetworkIdentity != null) && (NetworkIdentity.isLocalPlayer == false))
			{
				if (ActiveCharacter != null)
				{
					ActiveCharacter.GetComponentInChildren<Camera>().enabled = false;
				}

				return;
			}

			if (ActiveCharacter != null)
			{
				ActiveCharacter.Accelerate(GetAccelerationInput());
				ActiveCharacter.RotateCamera(GetRotationInput());
			}
		}

		private float GetAccelerationInput()
		{
			float value = 0;

			if (InputBindings != null)
			{
				if (Input.GetKey(InputBindings.MoveBackwardKey))
					value -= 1;
				if (Input.GetKey(InputBindings.MoveForwardKey))
					value +=1;
			}

			return value;
		}

		private float GetRotationInput()
		{
			float value = 0;

			if (InputBindings != null)
			{
				if (Input.GetKey(InputBindings.TurnLeftKey))
					value -= 1;
				if (Input.GetKey(InputBindings.TurnRightKey))
					value += 1;
			}

			return value;
		}
	}
}
