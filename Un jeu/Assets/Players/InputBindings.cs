﻿using UnityEngine;

namespace Assets.Players
{
	public class InputBindings : MonoBehaviour
	{
		public KeyCode MoveForwardKey;
		public KeyCode MoveBackwardKey;
		public KeyCode TurnLeftKey;
		public KeyCode TurnRightKey;
	}
}
