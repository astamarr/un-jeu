﻿using UnityEngine;

namespace Assets.Characters
{
	public class SphereCharacter : MonoBehaviour
	{
		public Rigidbody Rigidbody;
		public Transform RigidbodyTransform;
		public Transform CameraHandleTransform;

		public float AccelerationMultiplier = 10;
		public float RotationMultiplier = 10;

		public void Update()
		{
			CameraHandleTransform.localPosition = RigidbodyTransform.localPosition;
		}

		public void Accelerate(float accelerationInput)
		{
			Vector3 acceleration = Vector3.forward * accelerationInput * AccelerationMultiplier;
			Rigidbody.AddForce(CameraHandleTransform.localRotation * acceleration);
		}

		public void RotateCamera(float rotationInput)
		{
			float rotation = rotationInput * RotationMultiplier * Time.deltaTime;
			CameraHandleTransform.Rotate(Vector3.up * rotation);
		}
	}
}
